#!/bin/bash

function log () {
    printf "\e[32m<$(date --rfc-3339=seconds)>\e[0m - ${*}\n"
}


branchName=""
commitMessage=""

if [[ -z $1 ]] || [[ $1 == "-" ]]; then
    branchName=$(git rev-parse --abbrev-ref HEAD)
    if [[ -z $branchName ]]; then
        logAndExit "Could not get a branch name from HEAD, exiting!"
    fi
    log "Using current branch ${branchName}"
else
    branchName=$1
fi

if [[ ! -z $2 ]]; then
    commitMessage="${@:2}"
fi

git show-branch "${branchName}" > /dev/null 2>&1

if [[ $? != "0" ]]; then
    git checkout -b $branchName || logAndExit "Could not checkout new branch"
    log "Created branch ${branchName}"
else
    if [[ $(git rev-parse --abbrev-ref HEAD) != $branchName ]]; then
        git checkout $branchName || logAndExit "Could not checkout branch ${branchName}! Exiting..."
        log "Checked out ${branchName}"
    else
        log "Already on ${branchName}"
    fi
fi

if [[ -z $commitMessage ]]; then
    log "Add files and enter 'yes' when ready to commit..."
    read commitConfirmation

    if [[ $commitConfirmation != "yes" ]]; then
        log "Exiting!"
        exit 0
    fi

    log "Commit message:"
    read commitMessage
fi

git add .
git commit -m "${commitMessage}"

log "Committed!"

git push > ~/tmp/quickCommitMessage.txt

if [[ $? != "0" ]]; then
    git push --set-upstream origin $branchName > ~/tmp/quickCommitMessage.txt 2>&1
    log "Created new branch at origin"
else
    log "Pushed to active branch"
fi
